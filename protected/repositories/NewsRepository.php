<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 19.08.2021
 * Time: 21:26
 */


class NewsRepository
{
    /**
     * @param $id
     * @return News
     */
    public function get($id): News
    {
        if (!$post = News::model()->findByPk($id)) {
            throw new \DomainException('Post is not found.');
        }

        return $post;
    }

    /**
     * @param null $params
     * @return CActiveDataProvider
     */
    public function getAll($params = null)
    {
        $criteria = new CDbCriteria;

        if ($params !== null && is_array($params)) {
            $criteria->compare('id', $params['id']);
            $criteria->compare('content', $params['content'], true);
            $criteria->compare('at_date', $params['at_date'], true);

        }
        return new CActiveDataProvider('News', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array('at_date' => true),

            )
        ));
    }

    /**
     * @param News $post
     * @return bool
     */
    public function save(News $post)
    {
        return $post->save();
    }

    /**
     * @param News $post
     * @return bool
     * @throws CDbException
     */
    public function remove(News $post)
    {
        if (!$post->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        return true;
    }
}
