<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 19.08.2021
 * Time: 21:30
 */

class BaseController extends Controller
{
    /**
     * Используется для запросов ajax
     * @param $data
     */
    protected function responseAsJSON($data)
    {
        header('Content-type: application/json');
        echo CJSON::encode($data);

        // отключаем роуты лога, нам не нужно
        foreach (Yii::app()->log->routes as $route) {
            if ($route instanceof CWebLogRoute) {
                $route->enabled = false;
            }
        }
        Yii::app()->end();
    }

    protected function getModelErrors($model)
    {
        $result = [];
        foreach ($model->getErrors() as $attribute => $errors) {
            $result[CHtml::activeId($model, $attribute)] = $errors;
        }

        return $result;
    }
}
