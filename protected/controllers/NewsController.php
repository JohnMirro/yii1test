<?php

class NewsController extends BaseController
{
    private $repository;

    public function __construct($id, $module = null)
    {
        $this->repository = new NewsRepository();
        parent::__construct($id, $module);
    }

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'AjaxCreate'),
                'users' => array('*'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Создание поста
     * @throws CHttpException
     */
    public function actionAjaxCreate()
    {
        if (Yii::app()->request->isPostRequest && isset($_POST['News'])) {
            $model = new News;
            $model->attributes = $_POST['News'];
            if ($this->repository->save($model)) {
                $this->responseAsJSON(['success' => true]);
            }
            $this->responseAsJSON(['success' => false, 'errors' => $this->getModelErrors($model)]);
        } else {
            throw new CHttpException(400, 'bad request');
        }

    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new News;
        $dataProvider = $this->repository->getAll();
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => $model,
        ));
    }
}
