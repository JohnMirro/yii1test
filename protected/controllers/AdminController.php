<?php


class AdminController extends BaseController
{
    private $repository;

    public function __construct($id, $module = null)
    {
        $this->repository = new NewsRepository();
        parent::__construct($id, $module);
    }

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'ajaxdelete', 'ajaxload', 'ajaxupdate'),
                'users' => array('admin'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $model = new News('search');
        $model->unsetAttributes();  // clear any default values

        $dataProvider = $this->repository->getAll($_GET['News'] ?? null);

        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Обновление записи
     * @throws CHttpException
     */
    public function actionAjaxUpdate()
    {
        if (Yii::app()->request->isPostRequest && isset($_POST['News'])) {
            $id = $_POST['News']['id'];
            $model = $this->repository->get($id);
            $model->attributes = $_POST['News'];
            if ($this->repository->save($model)) {
                $this->responseAsJSON(['success' => true]);
            }
            $this->responseAsJSON(['success' => false, 'errors' => $this->getModelErrors($model)]);
        } else {
            throw new CHttpException(400, 'bad request');
        }

    }

    /**
     * Загрузка формы для диалога редактирования
     * @throws CException
     * @throws CHttpException
     */
    public function actionAjaxLoad()
    {
        if (Yii::app()->request->isPostRequest && isset($_POST['id'])) {
            $id = Yii::app()->request->getPost('id');
            $model = $this->repository->get($id);
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            Yii::app()->clientscript->scriptMap['jquery.min.js'] = false;
            $this->responseAsJSON([
                'success' => true,
                'form' => $this->renderPartial('_form', array('model' => $model, 'id' => $id), true, true)
            ]);


        } else {
            throw new CHttpException(400, 'bad request');
        }
    }

    /**
     * Удаление записи
     * @return array
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionAjaxDelete()
    {
        if (Yii::app()->request->isPostRequest && isset($_POST['id'])) {
            $id = Yii::app()->request->getPost('id');
            $model = $this->repository->get($id);
            if ($this->repository->remove($model)) {
                $this->responseAsJSON(['success' => true]);
            }
            $this->responseAsJSON(['success' => false]);
        } else {
            throw new CHttpException(400, 'bad request');
        }

    }

}
