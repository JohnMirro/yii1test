<?php

class m210817_133257_init extends CDbMigration
{
    public function up()
    {
        $this->createTable('tbl_news', array(
            'id' => 'pk',
            'content' => 'text',
            'at_date' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ));
    }

    public function down()
    {
        $this->dropTable('tbl_news');
    }
}
