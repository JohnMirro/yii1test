<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'news-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <label>Post your comment</label>
        <?php echo $form->textArea($model, 'content', array('rows' => 3, 'cols' => 25)); ?>
        <?php echo $form->error($model, 'content'); ?>
    </div>


    <div class="row buttons">
        <?php
        echo CHtml::ajaxSubmitButton('Post', array('AjaxCreate'),
            array(
                "success" => "function(r){
                                    if(r.success){
                                        hideFormErrors(form='#news-form');
                                        ws.emit('admin',{'message_name':'new_post'}); 
                                        $('#news-form').trigger('reset');
                                        updateList();
                                    }else{
                        	            showFormErrors(r.errors,form='#news-form');
                    	            }
                                }",
                "type" => "post",
                "dataType" => "json"
            ),
            array('type' => 'submit', 'id' => 'postBtn'));
        ?>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
