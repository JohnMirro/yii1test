<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="view" id="post_<?=$data->id?>">

	<?php echo CHtml::encode($data->content); ?>
	<br />

	<?php echo CHtml::encode($data->at_date); ?>

</div>
