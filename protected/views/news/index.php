<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'News',
);

?>

<h1>News feed</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'id'=>'itemList',
    'itemView' => '_view',
)); ?>

<?php
$this->beginClip('sidebar');
$this->renderPartial('_form', array('model'=>$model));
$this->endClip();
?>
