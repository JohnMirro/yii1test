<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('index'),
	'Create',
);

?>

<h1>Create News</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
