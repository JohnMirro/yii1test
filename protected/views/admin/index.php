<?php
/* @var $this AdminController */

$this->breadcrumbs = array(
    'Admin',
);

?>
<h1>Posts</h1>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'id' => 'itemList',
    'itemView' => '_view',
)); ?>

<!-- update form dialog -->
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'myDialog',
    'options' => array(
        'title' => 'Update dialog',
        'autoOpen' => false,
        'modal' => true,
        'resizable' => false,
        'width' => 550,
    ),
)); ?>
<div class="update-dialog-content"></div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
