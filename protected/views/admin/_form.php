<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
/* @var $id int */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'news-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <label>Update comment</label>
        <?php echo $form->textArea($model, 'content', array('rows' => 3, 'cols' => 25)); ?>
        <?php echo $form->error($model, 'content'); ?>
    </div>
    <?php echo $form->hiddenField($model, 'id', array('value' => $id)); ?>

    <div class="row buttons">
        <?php
        echo CHtml::ajaxSubmitButton('Update', array('AjaxUpdate'),
            array(
                "success" => "function(r){
                                    if(r.success){
                                        hideFormErrors(form='#news-form');
                                        ws.emit('admin',{'message_name':'refresh_list'}); 
                                        $('#news-form').trigger('reset');
                                        $('#myDialog .update-dialog-content').html('');
                                        $('#myDialog').dialog('close');
                                        updateList();
                                    }else{
                        	            showFormErrors(r.errors,form='#news-form');
                    	            }
                                }",
                "type" => "post",
                "dataType" => "json"
            ),
            array('type' => 'submit', 'id' => 'postBtn'));
        ?>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
