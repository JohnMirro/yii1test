<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="view" id="post_<?= $data->id ?>">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::encode($data->id); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
    <?php echo CHtml::encode($data->content); ?>
    <br/>

    <b>Posted:</b>
    <?php echo CHtml::encode($data->at_date); ?>
    <br/>

    <?php echo CHtml::link('Update','#' ,array('data-id'=>$data->id,'class' => 'adminUpdateBtn')); ?>
    <?php echo CHtml::link('Delete','#' ,array('data-id'=>$data->id,'class' => 'adminDeleteBtn')); ?>

</div>
