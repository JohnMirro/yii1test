<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs = array(
    'Update ' . $model->id,
);


?>

<h1>Update News <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>
