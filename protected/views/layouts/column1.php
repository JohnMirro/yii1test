<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
    <div id="alert" title="Attention" style="display: none">
        <p>New post.</p>
    </div>
	<?php echo $content; ?>
</div><!-- content -->
<?php $this->endContent(); ?>
