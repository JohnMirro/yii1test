<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-17">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-7 last">
	<div id="sidebar">
	<?php
    echo $this->clips['sidebar'];
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>
