let ws = io.connect('ws://localhost:5000');
ws.on('connect', function (socket) {
    // console.log('connected to websocket');
});

let onevent = ws.onevent;
ws.onevent = function (packet) {
    let args = packet.data || [];
    onevent.call(this, packet);
    packet.data = ["*"].concat(args);
    onevent.call(this, packet)
};

/**
 * тут все и происходит
 */
ws.on("*", function (event, data) {
    // console.log('on ', event,data);
    switch (event) {
        case 'refresh_list':
            updateList();
            break;
        case 'delete_item':
            deletePost(data);
            break;
        case 'new_post':
            alertMessage();
            break;
    }
});


function deletePost(id) {
    if ($('#post_' + id).length > 0) {
        $('#post_' + id).remove();
    }
}

function updateList() {
    $.fn.yiiListView.update('itemList', {});
}

function alertMessage() {
    updateList();
    $("#alert").dialog();
}

function showFormErrors(data, form) {
    let summary = '';
    summary = "<p>Please solve following errors:</p>";

    $.each(data, function (key, val) {
        $(form + " #" + key + "_em_").html(val.toString());
        $(form + " #" + key + "_em_").show();
        $("#" + key).parent().addClass("row error");
        summary = summary + "<ul><li>" + val.toString() + "</li></ul>";
    });

    $(form + "_es_").html(summary.toString());
    $(form + "_es_").show();
}

function hideFormErrors(form) {
    $(form + "_es_").html('');
    $(form + "_es_").hide();
    $("[id$='_em_']").html('');
}

$(document).ready(function () {
    /**
     * Обработчик нажатия кнопки Update у админа
     */
    $(document).on('click', 'a.adminUpdateBtn', function (e) {
        e.preventDefault();
        $.ajax({
            success: function (r) {
                if (r.success) {
                    $('#myDialog .update-dialog-content').html(r.form);
                    $('#myDialog').dialog('open');
                }
            },
            error: function () {
                alert('Not successful')
            },
            type: 'post',
            url: '/admin/AjaxLoad',
            dataType: 'json',
            data: {id: $(this).attr('data-id')}

        })
    });

    /**
     * Обработчик нажатия кнопки Delete у админа
     */
    $(document).on('click', 'a.adminDeleteBtn', function (e) {
        e.preventDefault();
        let id = $(this).attr('data-id');
        $.ajax({
            success: function (r) {
                if (r.success) {
                    ws.emit('broadcast', {'message_name': 'delete_item', 'data': id});
                }
            },
            error: function () {
                alert('Not successful')
            },
            type: 'post',
            url: '/admin/AjaxDelete',
            dataType: 'json',
            data: {id: id}

        })
    });

});
