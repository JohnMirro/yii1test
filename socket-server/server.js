const server = require('socket.io')(5000, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

server.on('connection', function (socket) {

    socket.on('broadcast', function (message) {
        socket.broadcast.emit(message.message_name, message.data); // всем
        socket.emit(message.message_name, message.data); // ну и себе
    });
    socket.on('admin', function (message) {
        socket.broadcast.emit(message.message_name); // всем
    });

    socket.on('disconnect', function () {
        console.log('disconnected...')
    })
});
